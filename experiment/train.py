import argparse
import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.impute import SimpleImputer
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import OneHotEncoder, StandardScaler

# Parse argument
ap = argparse.ArgumentParser()
ap.add_argument('-o', '--output', default="model.pkl")
args = vars(ap.parse_args())

# Set random seed
seed = 46

"""
################################
########## DATA PREP ###########
################################
"""

# Load in the data
df = pd.read_csv("data/train.csv")

# Specify column type
num_col = ["Pclass", "Age", "SibSp", "Parch", "Fare"]
cat_col = ["Sex"]

# Split into train and test sections
X = df.drop("Survived", axis=1)
y = df.Survived
X_train, X_test, y_train, y_test = train_test_split(
    df, y, test_size=0.2, random_state=seed
)

"""
#################################
########## MODELLING ############
#################################
"""

# Create simple pipeline
num_pipeline = make_pipeline(
    SimpleImputer(strategy="median"),
    StandardScaler()
    )

cat_pipeline = make_pipeline(OneHotEncoder())

# Join categorical and numeric features
data_pipeline = ColumnTransformer(
    [("pipe_num", num_pipeline, num_col), ("pipe_cat", cat_pipeline, cat_col)]
)

# Make final pipeline
mlpipe = make_pipeline(
    data_pipeline,
    RandomForestClassifier(random_state=seed)
    )

# Fit a model on the train section
mlpipe.fit(X_train, y_train)

# Report training set score
train_score = mlpipe.score(X_train, y_train) * 100
# Report test set score
test_score = mlpipe.score(X_test, y_test) * 100


# Write scores to a file
with open("report/metrics.txt", "w") as outfile:
    outfile.write("Training Accuracy: %2.1f%%\n" % train_score)
    outfile.write("Testing Accuracy: %2.1f%%\n" % test_score)


"""
##########################################
##### PLOT FEATURE IMPORTANCE ############
##########################################
"""

# Calculate feature importance in random forest
importances = mlpipe["randomforestclassifier"].feature_importances_
labels = df.columns
feature_df = pd.DataFrame(
    list(zip(labels, importances)), columns=["feature", "importance"]
)
feature_df = feature_df.sort_values(
    by="importance",
    ascending=False,
)

# image formatting
axis_fs = 18  # fontsize
title_fs = 22  # fontsize
sns.set(style="whitegrid")

ax = sns.barplot(x="importance", y="feature", data=feature_df)
ax.set_xlabel("Importance", fontsize=axis_fs)
ax.set_ylabel("Feature", fontsize=axis_fs)  # ylabel
ax.set_title("Random forest\nfeature importance", fontsize=title_fs)

plt.tight_layout()
plt.savefig("report/feature_importance.png", dpi=120)
plt.close()


"""
##########################################
######## PLOT CONFUSION MATRIX ###########
##########################################
"""

y_pred = mlpipe.predict(X_test)
cf = confusion_matrix(y_test, y_pred)
group_names = ["True Neg", "False Pos", "False Neg", "True Pos"]
group_counts = ["{0:0.0f}".format(value) for value in cf.flatten()]
group_percentages = [
    "{0:.2%}".format(value) for value in cf.flatten() / np.sum(cf)
    ]
labels = [
    f"{v1}\n{v2}\n{v3}"
    for v1, v2, v3 in zip(group_names, group_counts, group_percentages)
]
labels = np.asarray(labels).reshape(2, 2)

ax = sns.heatmap(cf, annot=labels, fmt="", cmap="Blues")
ax.set_title("Confusion Matrix", fontsize=title_fs)

plt.tight_layout()
plt.savefig("report/confusion_matrix.png", dpi=120)

# classification report
report = classification_report(y_test, y_pred)
# Append scores to a file
with open("report/metrics.txt", "a") as outfile:
    outfile.write(f"Classification Report:\n ```\n{report}\n```\n")

"""
##########################################
############## SAVE MODEL ################
##########################################
"""

with open(args['output'], "wb") as model_file:
    pickle.dump(mlpipe, model_file)
